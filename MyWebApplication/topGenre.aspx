﻿<%@ Page Title="topGenre" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="topGenre.aspx.cs" Inherits="MyWebApplication.topGenre" %>

<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
   
           <asp:Chart ID="Chart1" runat="server" Width="800px" Height="800px" >
            <Titles>
                <asp:Title Text="Percentage of movie genres in database" Font="Microsoft Sans Serif, 15pt"></asp:Title>
            </Titles>
            <series>
                <asp:Series Name="Series1" ChartArea="ChartArea1" ChartType="Pie" LabelForeColor="Transparent" >
                    <%--property LabelForeColor="Transparent" -> aby podpisy na samym diagramie kołowym były niewidoczne--%>
                </asp:Series>
            </series>
            <chartareas>
                <asp:ChartArea Name="ChartArea1" Area3DStyle-Rotation="2" Area3DStyle-Inclination="0" Area3DStyle-Enable3D="True" Area3DStyle-LightStyle="Simplistic" Area3DStyle-Perspective="10" Area3DStyle-IsRightAngleAxes="true">
                    <AxisX Title="Genre Name" >  </AxisX>
                    <AxisY Title="Total Percentage" ></AxisY>
                </asp:ChartArea>
            </chartareas>
               <Legends>
                   <asp:Legend BackColor="Transparent" Alignment="Center" Docking="Right" IsTextAutoFit="true" Name="Default" LegendStyle="Column" ></asp:Legend>

               </Legends>
        </asp:Chart>
    
    </asp:Content>
