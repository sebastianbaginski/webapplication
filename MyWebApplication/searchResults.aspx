﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="searchResults.aspx.cs" Inherits="MyWebApplication.searchResults" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:GridView ID="GridView1" runat="server" Height="213px" Width="372px">

            <Columns>

                   <asp:BoundField DataField="movie.original_title" HeaderText="Tytuł" ItemStyle-Width="250" />
                   <asp:BoundField DataField="genre.name" HeaderText="Gatunek" ItemStyle-Width="120" />
                   <asp:BoundField DataField="movie.vote_average" HeaderText="Średnia ocen" ItemStyle-Width="60" />
                </Columns>

        </asp:GridView>
    
    </div>
    </form>
</body>
</html>
