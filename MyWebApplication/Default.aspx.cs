﻿using System;
using System.Data;
using Npgsql;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;

namespace MyWebApplication
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {


                var connectionFromConfiguration = WebConfigurationManager.ConnectionStrings["DBConnection"];

                using (NpgsqlConnection dbConnection = new NpgsqlConnection(connectionFromConfiguration.ConnectionString))
                {
                    try
                    {
                        dbConnection.Open();
                        //ltConnectionMessage.Text = "Połączenie z bazą: Successful.";
                        string query1 = "select original_title, vote_average from movie order by vote_average DESC, release_date ASC limit 20;";
                        NpgsqlCommand cmd = new NpgsqlCommand(query1, dbConnection);
                        NpgsqlDataReader reader = cmd.ExecuteReader();

                        GridView1.DataSource = reader;
                        GridView1.DataBind();
                    }
                    catch (NpgsqlException ex)
                    {
                        //ltConnectionMessage.Text = "Connection failed: " + ex.Message;
                    }
                    finally
                    {
                        dbConnection.Close();
                        dbConnection.Dispose();
                    }
                }
            }

        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}