﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWebApplication
{
    public partial class searchResults : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            var connectionFromConfiguration = WebConfigurationManager.ConnectionStrings["DBConnection"];
            using (NpgsqlConnection dbConnection = new NpgsqlConnection(connectionFromConfiguration.ConnectionString))
            {
                //TextBox txt1 = (TextBox)Page.PreviousPage.FindControl("VoteSearch");
                //TextBox txt2 = (TextBox)Page.PreviousPage.FindControl("GenreSearch");

                string query3 = ("SELECT movie.original_title, genre.name, movie.vote_average FROM movie, movie_genre LEFT JOIN genre ON movie_genre.genre_id=genre.id WHERE movie.vote_average >= " + txt1 + " AND genre.name = " + txt2 + " ORDER BY movie.vote_average DESC;");
                NpgsqlCommand cmd = new NpgsqlCommand(query3, dbConnection);

                dbConnection.Open();
                NpgsqlDataReader reader = cmd.ExecuteReader();
                
                GridView1.DataBind();
                dbConnection.Close();
                dbConnection.Dispose();
            }

        }
    }
}