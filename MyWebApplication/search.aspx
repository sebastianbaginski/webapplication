﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="search.aspx.cs" Inherits="MyWebApplication.search" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="server">

    <fieldset>
        <legend>Search</legend>
        Movie genre: <asp:TextBox ID="GenreSearch" runat="server" ></asp:TextBox>
        Minimal vote average: <asp:TextBox ID="VoteSearch" runat="server"></asp:TextBox>
        <asp:Button ID="Submit" runat="server" Text="Search" PostBackUrl="~/searchResults.aspx" /> <br />
    </fieldset>




    
</asp:Content>
