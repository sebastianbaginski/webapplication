﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Configuration;
using Npgsql;

namespace MyWebApplication
{
    public partial class topGenre : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GetChartData();
        }

        private void GetChartData()
        {
            var connectionFromConfiguration = WebConfigurationManager.ConnectionStrings["DBConnection"];
            using (NpgsqlConnection dbConnection = new NpgsqlConnection(connectionFromConfiguration.ConnectionString))
            {
                string query2 = "SELECT genre.name,COUNT(movie_genre.movie_id) AS CountOfMoviesPerGenre FROM movie_genre LEFT JOIN genre ON movie_genre.genre_id=genre.id GROUP BY genre.name ORDER BY CountOfMoviesPerGenre DESC;";
                NpgsqlCommand cmd = new NpgsqlCommand(query2, dbConnection);

                Series series = Chart1.Series["Series1"];
                Chart1.Series["Series1"].Label = "#VALX" + "\n" + "#PERCENT";
                
                //Chart1.ChartAreas[0].InnerPlotPosition.X = 0;
                //Chart1.ChartAreas[0].InnerPlotPosition.Y = 0;

                //Chart1.ChartAreas[0].InnerPlotPosition.Height = 100;
                //Chart1.ChartAreas[0].InnerPlotPosition.Width = 100;

                dbConnection.Open();
                NpgsqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    series.Points.AddXY(reader["name"].ToString(), reader["CountOfMoviesPerGenre"]);
                }
                Chart1.DataBind();
                dbConnection.Close();
                dbConnection.Dispose();
            }
           
            



        }
    }
}