﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MyWebApplication._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Main Page</h2>
    <hr />
    <h4>
        <asp:Literal ID="ltConnectionMessage" runat="server" />
    </h4>
    <%--<div class="row">
        <ul> czyli unordered list
            <asp:Literal ID="ltOutput" runat="server" />--%>

            <asp:GridView ID="GridView1" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" AutoGenerateColumns="false">
                <AlternatingRowStyle BackColor="White" />
                <EditRowStyle BackColor="#2461BF" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#EFF3FB" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                <SortedDescendingHeaderStyle BackColor="#4870BE" />

                <Columns>

                   <asp:BoundField DataField="original_title" HeaderText="Tytuł" ItemStyle-Width="250" />
                   <asp:BoundField DataField="vote_average" HeaderText="Średnia ocen" ItemStyle-Width="60" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HyperLink runat="server" NavigateUrl='<%# string.Format("http://www.omdbapi.com/?t={0}&y=&plot=short", HttpUtility.UrlEncode(Eval("original_title").ToString()))
                                 %>'
                                Text="Link do OMDB" />
                        </ItemTemplate>

                    </asp:TemplateField>
                </Columns>

            </asp:GridView>
        <%--</ul>

    </div>--%>

    </asp:Content>
